// SPDX-License-Identifier: MIT


pragma solidity ^0.8.18;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Voting is Ownable {
    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    WorkflowStatus public status;
    uint public winningProposalId;
    uint public registrationEndTime;
    uint public proposalsEndTime;
    uint public votingEndTime;

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
    }

    struct Proposal {
        string description;
        uint voteCount;
    }

    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted(address voter, uint proposalId);

    mapping(address => Voter) public voters;
    Proposal[] public proposals;

    constructor() {
        status = WorkflowStatus.RegisteringVoters;
        winningProposalId = 0;
    }

    function startProposalsRegistration(uint _registrationDuration) public onlyOwner {
        require(status == WorkflowStatus.RegisteringVoters, "Not in the correct state");
        status = WorkflowStatus.ProposalsRegistrationStarted;
        registrationEndTime = block.timestamp + _registrationDuration;
        emit WorkflowStatusChange(WorkflowStatus.RegisteringVoters, WorkflowStatus.ProposalsRegistrationStarted);
    }

    function endProposalsRegistration(uint _proposalsDuration) public onlyOwner {
        require(status == WorkflowStatus.ProposalsRegistrationStarted, "Not in the correct state");
        status = WorkflowStatus.ProposalsRegistrationEnded;
        proposalsEndTime = block.timestamp + _proposalsDuration;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationStarted, WorkflowStatus.ProposalsRegistrationEnded);
    }

    function startVotingSession(uint _votingDuration) public onlyOwner {
        require(status == WorkflowStatus.ProposalsRegistrationEnded, "Not in the correct state");
        status = WorkflowStatus.VotingSessionStarted;
        votingEndTime = block.timestamp + _votingDuration;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationEnded, WorkflowStatus.VotingSessionStarted);
    }

    function endVotingSession() public onlyOwner {
        require(status == WorkflowStatus.VotingSessionStarted, "Not in the correct state");
        status = WorkflowStatus.VotingSessionEnded;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionStarted, WorkflowStatus.VotingSessionEnded);
    }

    function tallyVotes() public onlyOwner {
        require(status == WorkflowStatus.VotingSessionEnded, "Not in the correct state");
        status = WorkflowStatus.VotesTallied;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionEnded, WorkflowStatus.VotesTallied);

        uint winningCount = 0;
        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > winningCount) {
                winningCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }
    }

    function registerVoter(address _voter) public onlyOwner {
        require(status == WorkflowStatus.RegisteringVoters, "Not in the correct state");
        require(!voters[_voter].isRegistered, "Voter already registered");
        voters[_voter].isRegistered = true;
        emit VoterRegistered(_voter);
    }

    function submitProposal(string memory _description) public {
        require(status == WorkflowStatus.ProposalsRegistrationStarted, "Not in the correct state");
        uint proposalId = proposals.length;
        proposals.push(Proposal({
            description: _description,
            voteCount: 0
        }));
        emit ProposalRegistered(proposalId);
    }

    function vote(uint _proposalId) public {
        require(status == WorkflowStatus.VotingSessionStarted, "Not in the correct state");
        require(voters[msg.sender].isRegistered, "You are not a registered voter");
        require(!voters[msg.sender].hasVoted, "You have already voted");
        require(_proposalId < proposals.length, "Invalid proposal ID");
        require(block.timestamp < votingEndTime, "Voting session has ended");

        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = _proposalId;
        proposals[_proposalId].voteCount++;
        emit Voted(msg.sender, _proposalId);
    }

    function cancelVote() public {
        require(status == WorkflowStatus.VotingSessionStarted, "Not in the correct state");
        require(voters[msg.sender].hasVoted, "You have not voted yet");
        require(block.timestamp < votingEndTime, "Voting session has ended");

        uint votedProposalId = voters[msg.sender].votedProposalId;
        voters[msg.sender].hasVoted = false;
        voters[msg.sender].votedProposalId = 0; // Reset voted proposal ID
        proposals[votedProposalId].voteCount--; // Decrement the vote count
        emit Voted(msg.sender, 0); // Indicate vote cancellation
    }

    function revokeProposal(uint _proposalId) public {
        require(status == WorkflowStatus.ProposalsRegistrationStarted, "Not in the correct state");
        require(_proposalId < proposals.length, "Invalid proposal ID");
        require(!voters[msg.sender].hasVoted, "You have already voted");
        require(block.timestamp < proposalsEndTime, "Proposal registration has ended");

        // Remove the proposal and shift the array
        for (uint i = _proposalId; i < proposals.length - 1; i++) {
            proposals[i] = proposals[i + 1];
        }
        proposals.pop();
    }

    function getWinner() public view returns (string memory description, uint voteCount) {
        require(status == WorkflowStatus.VotesTallied, "Voting is not finished yet");
        description = proposals[winningProposalId].description;
        voteCount = proposals[winningProposalId].voteCount;
    }
}